﻿
namespace max.option.optiongroup
{
    public sealed class SoundOptions
    {
        /// <summary>
        /// <para>The volume level for game sound effects.</para>
        /// </summary>
        /// <value>Volume</value>
        public float SoundFXVolume { get; set; } = 0;
        /// <summary>
        /// <para>The volume level for game music.</para>
        /// </summary>
        /// <value>Volume</value>
        public float MusicVolume { get; set; } = 0;
        /// <summary>
        /// <para>The volume level for SoundEmitters.</para>
        /// </summary>
        /// <value>Volume</value>
        public float EnvironmentVolume { get; set; } = 1;

        /// <summary>
        /// <para>Cutoff distance factor. The larger this value the smaller the cutoff distance.</para>
        /// </summary>
        /// <value>Cutoff</value>
        public float EmitterCutoff { get; set; } = .3f;

        /// <summary>
        /// Whether or not the menu sounds will always play, or whether 
        /// they will only play when the maxseer engine is enabled.
        /// If it is true, it is always enabled, if it is false, it is 
        /// only playing sounds when maxseer is enabled.
        /// </summary>
        /// <value>Enabled/Disabled</value>
        public bool MenuSoundsEnabledAlways = true;
    }
}
