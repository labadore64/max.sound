﻿using max.sound.emitter;
using System;
using System.Collections.Generic;
using System.IO;
using max.option.optiongroup;
using max.geometry.shape2D;
using max.sound.path;
using max.sound.resource;
using max.sound.effect;
using Microsoft.Xna.Framework;
using max.sound.bank;

namespace max.sound.controller
{ 
    /// <summary>
    /// The main controller for sound.
    /// </summary>
    public class SoundController
    {
        #region Properties

        #region Direction Properties

        #region Positional properties

        /// <summary>
        /// The position of the listener.
        /// </summary>
        /// <value>Position</value>
        public Vector2 Position {
            get
            {
                return _position;
            }
            set
            {
                LastPosition = _position;
                _position = value;
                GenerateLook();
            }
        }

        Vector2 _position;

        /// <summary>
        /// The last position of the listener.
        /// </summary>
        /// <value>Position</value>
        public Vector2 LastPosition { get; private set; }

        #endregion

        #region Look Vector Properties

        /// <summary>
        /// The look vector, basically where the listener is looking.
        /// </summary>
        /// <value>Look Vector</value>
        public Vector2 LookVector
        {
            get
            { return _look; }
            set
            {
                _look = Vector2.Add(_position, value);
                _dist = Vector2.Distance(_position, _look);
                _angle = (float)Math.Atan2(_look.Y - Position.Y, _look.X - Position.X);
                LookLine = new Line2D(_position, _look);
            }
        }

        Vector2 _look;

        /// <summary>
        /// The line from the position of the sound listener to the look vector.
        /// </summary>
        /// <value>Look line</value>
        public Line2D LookLine { get; private set; }

        /// <summary>
        /// The angle of the look vector relative to the position
        /// </summary>
        /// <value>Look angle</value>
        public float LookAngle
        {
            get { return _angle; }
            set
            {
                _angle = value;
                HasRotated = true;
                GenerateLook();
            }
        }

        /// <summary>
        /// The look angle of the last frame.
        /// </summary>
        /// <value>Last look angle</value>
        public float LastLookAngle { get; private set; } = 0;

        float _angle = 0;

        /// <summary>
        /// The distance of the look vector.
        /// </summary>
        /// <value>Distance</value>
        public float LookDistance
        {
            get { return _dist; }
            set
            {
                _dist = value;
                HasRotated = true;
                GenerateLook();
            }
        }

        float _dist = 20;

        #endregion

        #endregion

        #region Movement Properties

        /// <summary>
        /// Whether or not the sound listener moved this frame.
        /// </summary>
        /// <value>True/False</value>
        public bool MovedThisFrame => HasMoved || HasRotated; 

        /// <summary>
        /// Whether or not the sound listener stopped moving this frame.
        /// </summary>
        /// <value>True/False</value>
        public bool ReleasedThisFrame => !MovedThisFrame && (HasMovedLast || HasRotatedLast); 

        /// <summary>
        /// Have you moved this frame?
        /// </summary>
        /// <value>True/False</value>
        public bool HasMoved => Position != LastPosition;

        /// <summary>
        /// Has the listener rotated this frame?
        /// </summary>
        /// <value>True/False</value>
        public bool HasRotated { get; private set; }


        // private values to track whether or not the listener moved last frame
        private bool HasMovedLast;
        private bool HasRotatedLast;

        #endregion

        #region Resources properties

        /// <summary>
        /// The sound resource manager.
        /// </summary>
        /// <value>Sound resource manager</value>
        public ISoundResourceManager ResourceManager { get; set; }

        /// <summary>The array of sound emitters.</summary>
        /// <value>Sound Emitters</value>
        public List<SoundEmitter> SoundEmitters { get; protected set; } = new List<SoundEmitter>();

        /// <summary>
        /// The sound banks loaded into memory.
        /// </summary>
        /// <value>Sound banks</value>
        public List<SoundBank> SoundBanks { get; protected set; } = new List<SoundBank>();

        /// <summary>
        /// The list of active sound paths.
        /// </summary>
        /// <value>Sound paths</value>
        public List<SoundPath> SoundPaths { get; protected set; } = new List<SoundPath>();
        /// <summary>
        /// The list of active sound path sequences.
        /// </summary>
        /// <value>Sound path sequences</value>
        public List<SoundPathSequence> SoundPathSequences { get; protected set; } = new List<SoundPathSequence>();

        /// <summary>
        /// The list of currently playing sound instances.
        /// </summary>
        /// <value>Sound instances</value>
        public List<MaxSoundInstance> SoundInstances { get; protected set; } = new List<MaxSoundInstance>();

        /// <summary>
        /// The list of geometries used for collisions.
        /// </summary>
        /// <value>Collisions</value>
        public List<I2DGeometry> Collisions { get; protected set; } = new List<I2DGeometry>();

        #endregion

        /// <summary>
        /// Options for sounds.
        /// </summary>
        /// <value>Options</value>
        public SoundOptions Options { get; private set; } = new SoundOptions();

        #endregion

        #region Controller Main
        /// <summary>
        /// Initializes the sound controller.
        /// </summary>
        public void Initialize()
        {

        }
        /// <summary>
        /// Updates the sound controller. Should be called once every cycle.
        /// </summary>
        public void Update()
        {
            ResourceManager.Update();
            SoundUpdates();
            SoundEmittersUpdate();
            UpdateSoundPaths();
            if(Position != LastPosition)
            {
                LastPosition = Position;
            }
            if(LastLookAngle != LookAngle)
            {
                LastLookAngle = LookAngle;
            }
            TestDisposedSfx();
        }

        /// <summary>
        /// Clears the state of the controller - stops all sounds as well.
        /// </summary>
        public void Clear()
        {
            StopSoundEffects();
            ClearEntities();
        }

        /// <summary>
        /// Clears all the entities except the currently playing sounds.
        /// </summary>
        public void ClearEntities()
        {
            ClearEmitters();
            ClearSoundPaths();
            ClearSoundPathSequences();
        }

        #endregion

        #region Sound Emitters

        /// <summary>
        /// Creates a sound emitter and adds it to the list.
        /// </summary>
        /// <returns>Sound emitter</returns>
        public SoundEmitter CreateEmitter(MaxSoundInstance sound)
        {
            sound.IsLooped = true;
            SoundEmitter soundEmitter = new SoundEmitter(this,sound);
            SoundEmitters.Add(soundEmitter);
            soundEmitter.Initialize();
            return soundEmitter;
        }

        /// <summary>
        /// Creates a sound emitter and adds it to the list.
        /// </summary>
        /// <returns>Sound emitter</returns>
        public SoundEmitter CreateEmitter(IMaxSound sound)
        {
            return CreateEmitter(sound.CreateInstance());
        }

        /// <summary>
        /// Creates a sound emitter and adds it to the list.
        /// </summary>
        /// <returns>Sound name</returns>
        public SoundEmitter CreateEmitter(string BankName, string Sound)
        {
            return CreateEmitter(CreateSoundInstance(BankName, Sound));
        }

        /// <summary>
        /// Removes the specified emitter from the emitter list.
        /// </summary>
        /// <param name="Emitter">Emitter</param>
        public void RemoveEmitter(SoundEmitter Emitter)
        {
            Emitter.Dispose();
            SoundEmitters.RemoveAt(SoundEmitters.IndexOf(Emitter));
        }

        /// <summary>
        /// Clears all the emitters that have been loaded.
        /// </summary>
        public void ClearEmitters()
        {
            for(int i = 0; i < SoundEmitters.Count; i++)
            {
                RemoveEmitter(SoundEmitters[i]);
                i--;
            }
        }

        // updates the state of the sound emitter, is called by Update()
        private void SoundEmittersUpdate()
        {
            for(int i = 0; i < SoundEmitters.Count; i++)
            {
                SoundEmitters[i].Update();
            }
        }
        #endregion

        #region Sound Effect Management

        /// <summary>
        /// Loads all sounds in a directory into memory, so they can be easily used.
        /// </summary>
        /// <param name="DirectoryName">The directory where the sounds are stored</param>
        /// <param name="Recursive">Whether or not to load files recursively</param>
        public void LoadSoundEffects(string DirectoryName, bool Recursive)
        {
            SoundBank Bank = new SoundBank(this);

            Bank.LoadSounds(DirectoryName, Recursive);
            SoundBanks.Add(Bank);
        }

        /// <summary>
        /// Loads all sound files from the given directory. Searches recursively.
        /// </summary>
        /// <param name="DirectoryName">The directory where the sounds are stored</param>
        /// <param name="Content">Content Manager</param>
        public void LoadSoundEffects(string DirectoryName)
        {
            LoadSoundEffects(DirectoryName, true);
        }
        /// <summary>
        /// Removes sound effects from the loaded sounds.
        /// </summary>
        /// <param name="Directory">Directory to unload sounds from</param>
        public void RemoveSoundBank(string Directory)
        {
            SoundBank sb = SoundBanks.Find(x => x.DirectoryName == Directory);
            sb.Dispose();
            SoundBanks.Remove(sb);
        }

        /// <summary>
        /// Gets a specific sound effect.
        /// </summary>
        /// <param name="SoundName">The name to search for</param>
        public IMaxSound GetSoundEffect(string BankName, string SoundName)
        {
            SoundBank sb = SoundBanks.Find(obj => obj.DirectoryName.Contains(BankName));

            if(sb != null)
            {
                if (sb.ContainsKey(SoundName))
                {
                    return sb[SoundName];
                }
            }

            return null;
        }

        /// <summary>
        /// Gets a specific sound effect.
        /// </summary>
        /// <param name="SoundName">The name to search for</param>
        public IMaxSound GetSoundEffect(string filename)
        {
            string BankName = Path.GetDirectoryName(filename);
            string SoundName = Path.GetFileName(filename);
            SoundBank sb = SoundBanks.Find(obj => obj.DirectoryName.Contains(BankName));

            if (sb != null)
            {
                if (sb.ContainsKey(SoundName))
                {
                    return sb[SoundName];
                }
            }

            return null;
        }

        #endregion

        #region Sound Effects
        /// <summary>
        /// Gets an instance of a sound effect.
        /// </summary>
        /// <param name="Name">Name of the sound effect</param>
        /// <param name="BankName">Name of the sound bank</param>
        /// <returns>Instance of that sound</returns>
        public MaxSoundInstance CreateSoundInstance(string BankName, string Name)
        {
            IMaxSound sound = GetSoundEffect(BankName, Name);
            if (sound != null) { 
                MaxSoundInstance instance = sound.CreateInstance();
                SoundInstances.Add(instance);
                return instance;
            }

            return null;
        }

        /// <summary>
        /// Gets an instance of a sound effect.
        /// </summary>
        /// <param name="Name">Name of the sound effect</param>
        /// <returns>Instance of that sound</returns>
        public MaxSoundInstance CreateSoundInstance(string Name)
        {
            IMaxSound sound = GetSoundEffect(Name);
            if (sound != null)
            {
                MaxSoundInstance instance = sound.CreateInstance();
                SoundInstances.Add(instance);
                return instance;
            }

            return null;
        }

        /// <summary>
        /// Plays a sound once if no other instances of the sound are playing.
        /// Returns null if a sound is already playing.
        /// </summary>
        /// <param name="Name">Name of the sound effect</param>
        /// <param name="BankName">Name of the bank name</param>
        /// <returns>Instance of the sound.</returns>
        public MaxSoundInstance PlaySound(string BankName, string Name)
        {
            if (SoundInstances.Find(x => x.SoundResource.Name.Contains(Name)) == null)
            {
                MaxSoundInstance instance = CreateSoundInstance(BankName,Name);
                instance.IsLooped = false;
                instance.DisposeWhenStopped = true;
                instance.Play();
                return instance;
            }

            return null;
        }

        /// <summary>
        /// Plays a sound once if no other instances of the sound are playing.
        /// Returns null if a sound is already playing.
        /// </summary>
        /// <param name="Name">Name of the sound effect</param>
        /// <returns>Instance of the sound.</returns>
        public MaxSoundInstance PlaySound(string Name)
        {
            if (SoundInstances.Find(x => x.SoundResource.Name.Contains(Name)) == null)
            {
                MaxSoundInstance instance = CreateSoundInstance(Name);
                instance.IsLooped = false;
                instance.DisposeWhenStopped = true;
                instance.Play();
                return instance;
            }

            return null;
        }

        /// <summary>
        /// Stops all playing sound effects.
        /// </summary>
        public void StopSoundEffects()
        {
            for (int i = 0; i < SoundInstances.Count; i++)
            {
                StopSoundEffect(SoundInstances[i]);
                i--;
            }
        }

        /// <summary>
        /// Stops a specific sound effect instance.
        /// </summary>
        /// <param name="Sound">Sound effect instance</param>
        public void StopSoundEffect(MaxSoundInstance Sound)
        {
            Sound.Dispose();
            SoundInstances.RemoveAt(SoundInstances.IndexOf(Sound));
        }

        #endregion

        #region Sound Paths

        /// <summary>
        /// Creates a sound path.
        /// </summary>
        /// <param name="Path"></param>
        /// <param name="SoundEffectName">Sound effect name</param>
        /// <param name="FalloffDistance">How far away the sound can be heard.</param>
        /// <param name="Speed">How fast the sound path travels.</param>
        /// <returns>Sound path</returns>
        public SoundPath CreateSoundPath(Path2D Path, string SoundEffectName, float FalloffDistance,float Speed)
        {
            SoundPath sp = new SoundPath(this, Path, SoundEffectName, FalloffDistance,Speed);
            SoundPaths.Add(sp);
            return sp;
        }

        /// <summary>
        /// Removes a sound path currently running.
        /// </summary>
        /// <param name="SoundPath">Soundpath to remove</param>
        public void RemoveSoundPath(SoundPath SoundPath)
        {
            SoundPath.Dispose();
            int index = SoundPaths.IndexOf(SoundPath);
            if (index >= 0)
            {
                SoundPaths.RemoveAt(index);
            }
        }

        /// <summary>
        /// Creates a sound path sequence.
        /// </summary>
        /// <param name="Path"></param>
        /// <param name="SoundEffectName"></param>
        /// <returns>Sound path sequence.</returns>
        public SoundPathSequence CreateSoundPathSequence(Path2D Path, string SoundEffectName,float Speed)
        {
            SoundPathSequence sp = new SoundPathSequence(this, Path, SoundEffectName,Speed,50);
            SoundPathSequences.Add(sp);
            return sp;
        }

        /// <summary>
        /// Clears all sound paths.
        /// </summary>
        public void ClearSoundPaths()
        {
            SoundPaths.Clear();
        }

        /// <summary>
        /// Clears all sound path sequences.
        /// </summary>
        public void ClearSoundPathSequences()
        {
            SoundPathSequences.Clear();
        }

        /// <summary>
        /// Removes a sound path sequence currently running.
        /// </summary>
        /// <param name="SoundPathSequence"></param>
        public void RemoveSoundPathSequence(SoundPathSequence SoundPathSequence)
        {
            int index = SoundPathSequences.IndexOf(SoundPathSequence);
            if (index >= 0)
            {
                SoundPathSequences.RemoveAt(SoundPathSequences.IndexOf(SoundPathSequence));
            }
            SoundPathSequence.Dispose();
        }

        private void UpdateSoundPaths()
        {
            for(int i = 0; i < SoundPaths.Count; i++)
            {
                SoundPaths[i].Update();
            }
        }

        #endregion

        #region internal

        // updates the sound effect instances and is called by Update()
        private void SoundUpdates()
        {
            for (int i = 0; i < SoundInstances.Count; i++)
            {
                SoundInstances[i].Update();
            }
        }

        // updates the look vector
        private void GenerateLook()
        {
            float x = Position.X + (_dist * (float)Math.Cos(_angle));
            float y = Position.Y - (_dist * (float)Math.Sin(_angle));
            _look = new Vector2(x, y);

            LookLine = new Line2D(_position, _look);
        }

        // tests if sfx and emitters have been disposed
        private void TestDisposedSfx()
        {
            for(int i = 0; i< SoundInstances.Count; i++)
            {
                if (SoundInstances[i].Disposed)
                {
                    SoundInstances.RemoveAt(i);
                    i--;
                }
            }

            for (int i = 0; i < SoundEmitters.Count; i++)
            {
                if (SoundEmitters[i].Disposed)
                {
                    SoundEmitters.RemoveAt(i);
                    i--;
                }
            }
        }
        #endregion
    }
}
