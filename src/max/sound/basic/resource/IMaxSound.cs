﻿using max.sound.bank;
using max.sound.effect;

namespace max.sound.resource
{
    /// <summary>
    /// An interface that represents a sound effect in the sound bank.
    /// </summary>
    public interface IMaxSound
    {
        #region properties
        /// <summary>
        /// The source object containing the actual sound.
        /// </summary>
        /// <value>Source</value>
        object Source { get; }

        /// <summary>
        /// The name of the resource.
        /// </summary>
        /// <value>Name</value>
        string Name { get; }

        /// <summary>
        /// Parent resource manager.
        /// </summary>
        /// <value>Parent</value>
        ISoundResourceManager Parent { get; }

        SoundBank SoundBank { get; }

        #endregion

        #region functions
        /// <summary>
        /// Creates an instance of a sound effect.
        /// </summary>
        /// <returns>Sound instance</returns>
        MaxSoundInstance CreateInstance();

        /// <summary>
        /// Disposes of the resource.
        /// </summary>
        void Dispose();
        #endregion

    }
}
