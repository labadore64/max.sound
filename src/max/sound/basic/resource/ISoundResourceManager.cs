﻿using max.sound.bank;
using max.sound.controller;
using Microsoft.Xna.Framework;

namespace max.sound.resource
{
    /// <summary>
    /// This interface represents the sound resource manager, a component that manages the sound
    /// assets so that you can load them into the game. It also lets you manage the objects used
    /// for 3D sound such as emitters and a sound listener.
    /// </summary>
    public interface ISoundResourceManager
    {

        #region Properties
        /// <summary>
        /// Parent sound controller.
        /// </summary>
        /// <value>Sound controller</value>
        SoundController SoundController { get; set; }

        /// <summary>
        /// Generic reference to the source listener.
        /// </summary>
        /// <value>Listener</value>
        object SourceListener { get; }

        /// <summary>
        /// The root directory from where you load the sound assets.
        /// </summary>
        /// <value>Directory name</value>
        string RootDirectory { get; }

        #endregion

        #region Asset management

        /// <summary>
        /// Gets a sound asset by name.
        /// </summary>
        /// <param name="AssetName">Name of the asset</param>
        /// <param name="Bank">Sound Bank</param>
        /// <returns>Asset (must be cast)</returns>
        IMaxSound GetSoundAsset(SoundBank Bank, string AssetName);

        /// <summary>
        /// Returns an instance of the sound emitter type used internally.
        /// </summary>
        /// <returns>Source sound emitter</returns>
        object GenerateEmitter();

        #endregion

        #region Controller

        /// <summary>
        /// Updates an emitter's position.
        /// </summary>
        /// <param name="Emitter">Emitter</param>
        /// <param name="Position">Position</param>
        void UpdateEmitterPosition(object Emitter, Vector3 Position);

        /// <summary>
        /// Updates the state of 3D sound components, such as the listener
        /// or static emitter properties.
        /// </summary>
        void Update();

        #endregion
    }
}
