﻿using max.geometry.shape2D;
using max.serialize;
using max.sound.controller;
using max.sound.effect;
using Microsoft.Xna.Framework;
using System;

namespace max.sound.emitter
{
    /// <summary>
    /// This interface represents a sound emitter.
    /// </summary>
    public class SoundEmitter : IMaxSerializable
    {
        #region Properties

        private float DistanceMultiplier;

        /// <summary>
        /// The source object for the emitter.
        /// </summary>
        object Emitter { get; set; }

        /// <summary>
        /// The source object for the listener.
        /// </summary>
        static object Listener { get; set; }

        // constants for serialization
        const string SER_NAME = "name";
        const string SER_POS = "pos";

        /// <summary>
        /// Whether or not this sound emitter has collision checking enabled. If true, it cannot be heard past walls.
        /// </summary>
        /// <value>Enabled/Disabled</value>
        public bool CollisionChecking { get; set; }

        /// <summary>
        /// The circle representing the area where the sound can be heard.
        /// </summary>
        /// <value>Circle</value>
        public Circle2D SoundArea
        {
            get
            {
                return _soundArea;
            }
            set
            {
                _soundArea = value;
                DistanceMultiplier = 1 / Distance;
            }
        }

        Circle2D _soundArea;

        /// <summary>
        /// The position of the sound emitter.
        /// </summary>
        /// <value>Position</value>
        public Vector2 Position
        {
            get
            {
                return SoundArea.Center;
            }
            set
            {
                SoundArea = new Circle2D(value, SoundArea.Radius);
                DistanceMultiplier = 1 / Distance;
            }
        }

        /// <summary>
        /// The cutoff distance of the sound emitter. Ouside of this radius,
        /// the sound volume is 0.
        /// </summary>
        /// <value>Distance</value>
        public float Distance
        {
            get
            {
                return SoundArea.Radius;
            }
            set
            {
                SoundArea = new Circle2D(SoundArea.Center,value);
                DistanceMultiplier = 1 / Distance;
            }
        }

        /// <summary>
        /// Whether or not the sound emitter has been disposed of.
        /// </summary>
        /// <value>True/False</value>
        public bool Disposed { get; private set; }
        /// <summary>
        /// The sound effect used with this emitter.
        /// </summary>
        /// <value>Sound effect instance</value>
        public MaxSoundInstance SoundEffect { get; private set; }


        /// <summary>
        /// The Sound Controller reference for this emitter.
        /// </summary>
        /// <value>Sound Controller</value>
        public SoundController SoundController { get; private set; }

        public Vector3 ApparentLocation
        {
            get
            {
                float angle = ListenerAngle + SoundController.LookAngle;
                return new Vector3(
                    (float)(SoundController.Position.X + ListenerDistance * Math.Cos(angle)),
                    (float)(SoundController.Position.Y + ListenerDistance * Math.Sin(angle)),
                    0);
            }
        }
        private float ListenerAngle
        {
            get
            {
                return (float)Math.Atan2(Position.Y - SoundController.Position.Y, Position.X - SoundController.Position.X);
            }
        }

        private float ListenerDistance
        {
            get
            {
                return Vector2.Distance(SoundController.Position, Position);
            }
        }

        public bool Serializable { get; set; }

        #endregion

        #region Constructors
        /// <summary>
        /// Empty constructor.
        /// </summary>
        public SoundEmitter()
        {

        }

        /// <summary>
        /// Initialize with a sound controller as a parent.
        /// </summary>
        /// <param name="Controller">Parent</param>
        public SoundEmitter(SoundController Controller)
        {
            SoundController = Controller;
        }

        /// <summary>
        /// Initialize with parent controller and sound instance.
        /// </summary>
        /// <param name="Controller">Sound controller</param>
        /// <param name="Sound">Sound instance</param>
        public SoundEmitter(SoundController Controller, MaxSoundInstance Sound)
        {
            SoundEffect = Sound;
            SoundController = Controller;
        }

        public SoundEmitter(SoundController Controller, MaxSerializedObject Data)
        {
            SoundController = Controller;
            Deserialize(Data);
        }

        #endregion

        #region Controller

        /// <summary>
        /// Initializes the sound emitter from properties from its parent.
        /// </summary>
        public void Initialize()
        {
            if(SoundController != null)
            {
                Emitter = SoundController.ResourceManager.GenerateEmitter();
                Listener = SoundController.ResourceManager.SourceListener;
            }
        }

        /// <summary>
        /// Updates the sound emitter's state for this cycle.
        /// </summary>
        public void Update()
        {
            if (SoundEffect != null)
            {
                if (!Disposed)
                {
                    //set volume for Sound.
                    SoundEffect.Volume = (float)(SoundController.Options.EnvironmentVolume * .1);

                    bool cando = true;

                    //to do: change the logic so it does collision checking through the collision controller.

                    if (CollisionChecking)
                    {
                        bool collisionDetected = false;

                        for (int i = 0; i < SoundController.Collisions.Count; i++)
                        {
                            if (SoundController.Collisions[i].IntersectPoints(SoundArea).Count > 0)
                            {
                                collisionDetected = true;
                                break;
                            }
                        }

                        if (collisionDetected)
                        {
                            cando = false;
                            SoundEffect.Volume = 0;
                            SoundEffect.Apply3D(SoundController.ResourceManager.SourceListener, Emitter);
                        }


                    }

                    if (cando)
                    {
                        SoundController.ResourceManager.UpdateEmitterPosition(Emitter, ApparentLocation);

                        if (Distance * SoundController.Options.EmitterCutoff < ListenerDistance * DistanceMultiplier)
                        {
                            SoundEffect.Volume = 0;
                        }

                        SoundEffect.Apply3D(SoundController.ResourceManager.SourceListener, Emitter);
                    }
                }
            }

        }
        /// <summary>
        /// Disposes of the sound resources used for this emitter. Should be called before dereferencing to avoid memory leaks.
        /// </summary>
        public void Dispose()
        {
            SoundEffect.Dispose();
            SoundController.StopSoundEffect(SoundEffect);
            Disposed = true;
        }

        #endregion

        #region sound control

        /// <summary>
        /// Plays the sound emitter's sound.
        /// </summary>
        public void Play()
        {
            SoundEffect.Play();
        }
        /// <summary>
        /// Pauses the sound emitter's sound.
        /// </summary>
        public void Pause()
        {
            SoundEffect.Pause();
        }
        /// <summary>
        /// Stops the sound emitter's sound.
        /// </summary>
        public void Stop()
        {
            SoundEffect.Stop();
        }

        #endregion

        #region Serialize

        public MaxSerializedObject Serialize()
        {
            MaxSerializedObject Data = new MaxSerializedObject();
            Data.SetType(this);
            Data.AddString(SER_NAME, SoundEffect.Name);
            Data.AddVector2(SER_POS, Position);
            return Data;
        }

        public void Deserialize(MaxSerializedObject Data)
        {
            if (SoundController != null) {

                string Sound = Data.GetString(SER_NAME);
                SoundEffect = SoundController.CreateSoundInstance(Sound);
                Position = Data.GetVector2(SER_POS);

                Initialize();
            }
        }

        public IMaxSerializable CreateInstance(MaxSerializedObject Data)
        {
            if (SoundController != null) {
                SoundEmitter emitter = new SoundEmitter(SoundController);
                emitter.Deserialize(Data);
                return emitter;
            }
            return null; 
        }

        #endregion
    }
}
