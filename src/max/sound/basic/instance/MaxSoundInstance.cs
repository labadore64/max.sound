﻿using max.serialize;
using max.sound.bank;
using max.sound.controller;
using max.sound.resource;

namespace max.sound.effect
{
    /// <summary>
    /// An abstract class that represents a template of 
    /// </summary>
    public abstract class MaxSoundInstance
    {

        #region Properties

        public enum SoundState {PLAYING, PAUSED, STOPPED}

        public abstract SoundState State { get; }

        /// <summary>
        /// Represents the ID for sounds in the serializer.
        /// </summary>
        protected const string SER_SOUND = "sound";

        /// <summary>
        /// The pitch of the sound effect.
        /// </summary>
        /// <value>Pitch</value>
        public virtual float Pitch { get; set; }
        /// <summary>
        /// The pan of the sound effect.
        /// </summary>
        /// <value>Pan</value>
        public virtual float Pan { get; set; }
        /// <summary>
        /// The volume of the sound effect.
        /// </summary>
        /// <value>Volume</value>
        public virtual float Volume { get; set; }
        /// <summary>
        /// The resource name of the sound effect.
        /// </summary>
        /// <value>Name</value>
        public virtual string Name { get; protected set; }

        /// <summary>
        /// The parent SoundBank.
        /// </summary>
        /// <value>Parent</value>
        public virtual SoundBank Parent { get; protected set; }
        /// <summary>
        /// Whether or not the sound effect is looping.
        /// </summary>
        /// <value>True/False</value>
        public virtual bool IsLooped { get; set; }
        /// <summary>
        /// Whether or not the sound effect is enabled.
        /// </summary>
        /// <value>True/False</value>
        public virtual bool Enabled { get; set; }

        /// <summary>
        /// Whether the sound has been disposed of.
        /// </summary>
        /// <value>True/False</value>
        public virtual bool Disposed { get; protected set; }

        /// <summary>
        /// Whether or not this instance disposes itself when it stops playing.
        /// </summary>
        public virtual bool DisposeWhenStopped { get; set; }

        /// <summary>
        /// The sound resource used for this insance.
        /// </summary>
        /// <value>Sound resource</value>
        public IMaxSound SoundResource { get; protected set; }

        public SoundController SoundController
        {
            get
            {
                return SoundResource.Parent.SoundController;
            }
        }

        #endregion

        #region Controller
        public abstract void Apply3D(params object[] args);

        public abstract void Update();
        public abstract void Dispose();

        #endregion

        #region Sound Control

        public abstract void Play();
        public abstract void Pause();
        public abstract void Stop();

        #endregion

        #region Other
        public abstract MaxSoundInstance Copy();
        #endregion
    }
}
