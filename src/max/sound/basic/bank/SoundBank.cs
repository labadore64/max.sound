﻿using max.sound.controller;
using max.sound.resource;
using System.Collections.Generic;
using System.IO;

namespace max.sound.bank
{
    /// <summary>
    /// A group of sound effects.
    /// </summary>
    public class SoundBank
    {
        /// <summary>
        /// The parent sound controller.
        /// </summary>
        /// <value>Sound controller</value>
        public SoundController SoundController { get; set; }

        /// <summary>
        /// The name of the directory where the sounds were loaded.
        /// </summary>
        /// <value>Directory</value>
        public string DirectoryName { get; protected set; }

        // the actual sounds
        Dictionary<string, IMaxSound> Sounds { get; set; } = new Dictionary<string, IMaxSound>();

        /// <summary>
        /// Sound dictionary.
        /// </summary>
        /// <param name="ID">Sound ID</param>
        /// <returns>Sound</returns>
        public IMaxSound this[string ID] => Sounds[ID];

        /// <summary>
        /// Whether or not this sound bank has been disposed of.
        /// </summary>
        /// <value>True/False</value>
        public bool Disposed { get; private set; }

        #region Constructors
        public SoundBank(SoundController Parent)
        {
            SoundController = Parent;
        }
        #endregion

        /// <summary>
        /// Loads the sounds into the sound bank.
        /// </summary>
        /// <param name="ID">ID of the sounds to load.</param>
        public void LoadSounds(string DirectoryName, bool Recursive)
        {
            this.DirectoryName = DirectoryName;
            if (Recursive)
            {
                string[] files = Directory.GetFiles(SoundController.ResourceManager.RootDirectory + "/" + DirectoryName, "*", SearchOption.AllDirectories);

                for (int i = 0; i < files.Length; i++)
                {
                    FileInfo file = new FileInfo(files[i]);
                    Sounds.Add(Path.GetFileNameWithoutExtension(file.Name), SoundController.ResourceManager.GetSoundAsset(this,Path.ChangeExtension(file.FullName, null)));
                }
            }
            else
            {
                DirectoryInfo dir = new DirectoryInfo(SoundController.ResourceManager.RootDirectory + "/" + DirectoryName);
                FileInfo[] files = dir.GetFiles("*.xnb");

                for (int i = 0; i < files.Length; i++)
                {
                    FileInfo file = files[i];
                    Sounds.Add(Path.GetFileNameWithoutExtension(file.Name), SoundController.ResourceManager.GetSoundAsset(this,Path.ChangeExtension(file.FullName, null)));
                }
            }
        }

        /// <summary>
        /// Checks if a key is in the sound dictionary.
        /// </summary>
        /// <param name="Key">Key to check</param>
        /// <returns>True/False</returns>
        public bool ContainsKey(string Key)
        {
            return Sounds.ContainsKey(Key);
        }

        /// <summary>
        /// Disposes of the sound bank and its sound effects.
        /// </summary>
        public void Dispose()
        {
            IMaxSound[] sounds = new IMaxSound[Sounds.Values.Count];
            Sounds.Values.CopyTo(sounds, 0);

            for(int i = 0; i < sounds.Length; i++)
            {
                sounds[i].Dispose();
            }
            Disposed = true;
        }

    }
}
