﻿using max.geometry.shape2D;
using max.sound.controller;
using max.sound.emitter;
using Microsoft.Xna.Framework;

namespace max.sound.path
{
    /// <summary>
    /// This class represents sound emitter travelling along a path.
    /// 
    /// The sound emitter travels along a path of nodes at a 
    /// constant rate.
    /// </summary>
    public class SoundPath
    {
        /// <summary>The path that the sound travels along.</summary>
        /// <value>Path2D</value>
        public Path2D TravelPath { get; protected set; }
        /// <summary>Start Position.</summary>
        Vector2 StartPosition;
        /// <summary>The speed that the path travels along. Defaults to 2.</summary>
        /// <value>Speed</value>
        public float PathSpeed { get; set; }

        /// <summary>Is this path used for accessibility purposes?</summary>
        /// <value>Is accessibility path</value>
        public bool AccessTestPath { get; set; }
        /// <summary>The sound emitter for this path.</summary>
        public SoundEmitter Sound;

        SoundController SoundController;

        /// <summary>
        /// Whether or not the path is travelling in the reverse direction or not.
        /// </summary>
        /// <value>True/False</value>
        public bool ReverseDirection { get; set; }

        /// <summary>
        /// The current position of the path.
        /// </summary>
        /// <value>Position</value>
        public Vector2 Position { get { return TravelPath.Position; } }

        /// <summary>
        /// Creates a new sound path.
        /// </summary>
        /// <param name="Path">Path</param>
        public SoundPath(SoundController SoundController, Path2D Path, string SoundFilePath, float SoundFalloffDistance, float Speed)
        {
            this.SoundController = SoundController;
            PathSpeed = Speed;
            if (Path.Lines.Length > 0)
            {
                StartPosition = Path.Lines[0].Points[0];
                SetTravelPath(Path2D.Copy(Path));
            }

            Sound = SoundController.CreateEmitter(SoundController.GetSoundEffect(SoundFilePath).CreateInstance());
            Sound.SoundArea = new Circle2D(StartPosition, SoundFalloffDistance);
            Sound.CollisionChecking = true;
        }

        /// <summary>
        /// Sets the travel path for this sound path.
        /// </summary>
        /// <param name="Points">Path represented as points</param>
        private void SetTravelPath(Path2D Path)
        {
            TravelPath = Path;
            TravelPath.Speed = PathSpeed;
            TravelPath.PathEndBehavior = Path2D.EndBehavior.RESTART;
        }
        /// <summary>
        /// Updates the sound path's state.
        /// </summary>
        public void Update()
        {
            if (ReverseDirection)
            {
                TravelPath.UpdateReverse();
            }
            else
            {
                TravelPath.Update();
            }
            Sound.Position = TravelPath.Position;
            Sound.Update();
        }

        /// <summary>
        /// Initializes the sound path.
        /// </summary>
        public void Start()
        {
            TravelPath.Start();
            Sound.Position = TravelPath.Position;
            Sound.Play();
        }
        /// <summary>
        /// Dispose of the sound path sound resources.
        /// </summary>
        public void Dispose()
        {
            SoundController.RemoveEmitter(Sound);
        }

        public override bool Equals(object obj)
        {
            if (obj is SoundPath) {
                SoundPath sp = (SoundPath)obj;

                Polygon2D poly = TravelPath.ToPolygon2D();

                if (poly.PointsEqual(sp.TravelPath.ToPolygon2D().Points) &&
                    PathSpeed == sp.PathSpeed &&
                    Position == sp.Position)
                {
                    return true;
                }
            }
            return false;
        }

        public override int GetHashCode()
        {
            int hashCode = 0;

            hashCode ^= PathSpeed.GetHashCode();
            hashCode ^= Sound.GetHashCode();
            hashCode ^= Position.GetHashCode();
            hashCode ^= TravelPath.GetHashCode();

            return hashCode;
        }
    }
}
