﻿using max.geometry.shape2D;
using max.sound.controller;
using max.sound.emitter;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;

namespace max.sound.path
{
    /// <summary>
    /// This class represents list of sound paths.
    /// 
    /// The sound paths are separated equally along the distance
    /// to give a steady stream of sounds travelling from the
    /// current location to the destination.
    /// </summary>
    public class SoundPathSequence
    {

        SoundPath[] SoundPaths;

        /// <summary>
        /// Whether or not the sound path is running.
        /// </summary>
        /// <value>True/False</value>
        public bool Active { get; set; }

        /// <summary>
        /// The distance that individual sounds are from each other.
        /// </summary>
        /// <value>Distance</value>
        public float SoundDistance { get; set; } = 50;

        /// <summary>
        /// The path that the sound path travels.
        /// </summary>
        /// <value>Path</value>
        public Path2D TravelPath {get; protected set;}

        /// <summary>
        /// The speed that the path travels at.
        /// </summary>
        /// <value>Speed</value>
        public float PathSpeed { get; protected set; }

        /// <summary>
        /// The path of the sound effect used for the sound paths.
        /// </summary>
        /// <value>Sound effect filepath</value>
        public string SoundEffectFilePath { get; protected set; } = "";

        /// <summary>
        /// The parent sound controller.
        /// </summary>
        /// <value>Sound Controller</value>
        public SoundController SoundController { get; protected set; }

        /// <summary>
        /// Gets the points of all the paths in the sequence as an array.
        /// </summary>
        /// <value>Positions</value>
        public Vector2[] Points
        {
            get
            {
                Vector2[] returner = new Vector2[SoundPaths.Length];
                SoundEmitter xse;
                for(int i = 0; i < SoundPaths.Length; i++)
                {
                    xse = SoundPaths[i].Sound;
                    returner[i] = new Vector2(xse.Position.X, xse.Position.Y);
                }
                return returner;
            }
        }

        public SoundPathSequence()
        {

        }

        /// <summary>
        /// Create a new instance using a path, and using the spacing distance for the emitters as defined in MaxSeerOptions.Sound.SoundPathSpeed.
        /// </summary>
        /// <param name="Path">Path</param>
        public SoundPathSequence(SoundController SoundController, Path2D Path, string SoundEffectFilePath, float PathSpeed, float SoundDistance)
        {
            this.SoundController = SoundController;
            Path.PathEndBehavior = Path2D.EndBehavior.RESTART;
            TravelPath = Path;
            this.SoundEffectFilePath = SoundEffectFilePath;
            this.PathSpeed = PathSpeed;
            this.SoundDistance = SoundDistance;
        }

        /// <summary>
        /// Starts the sound path sequence.
        /// </summary>
        public virtual void Start()
        {
            int count = (int)Math.Floor(TravelPath.Distance / SoundDistance);
            float space = 1f / count;
            SoundPath p;

            List<SoundPath> pz = new List<SoundPath>();

            for (int i = 0; i < count; i++)
            {
                p = SoundController.CreateSoundPath((Path2D)TravelPath.Copy(), SoundEffectFilePath, SoundDistance, PathSpeed);
                p.TravelPath.SetPositionPercent(space * i);
                p.Start();
                pz.Add(p);
            }
            SoundPaths = pz.ToArray();
            Active = true;
        }

        /// <summary>
        /// Pauses the movement of the sound path.
        /// </summary>
        public virtual void PauseMovement()
        {
            Active = false;
        }

        /// <summary>
        /// Disposes the sound path sequence so its resources are released.
        /// </summary>
        public virtual void Dispose()
        {
            if (SoundPaths != null)
            {
                for (int i = 0; i < SoundPaths.Length; i++)
                {
                    SoundController.RemoveSoundPath(SoundPaths[i]);
                }
            }
        }

        public override bool Equals(object obj)
        {
            if(obj is SoundPathSequence)
            {
                SoundPathSequence sps = (SoundPathSequence)obj;

                if (sps.SoundPaths.Length != SoundPaths.Length)
                {
                    return false;
                }

                if(sps.PathSpeed == PathSpeed &&
                    sps.SoundEffectFilePath == SoundEffectFilePath)
                {
                    for(int i = 0; i < SoundPaths.Length; i++)
                    {
                        if(SoundPaths[i] != sps.SoundPaths[i])
                        {
                            return false;
                        }
                    }

                    return true;
                }
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
