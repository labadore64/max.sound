﻿using max.sound.controller;
using max.sound.resource;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;

namespace max.sound.xna
{
    /// <summary>
    /// This interface represents the sound resource manager, a component that manages the sound
    /// assets so that you can load them into the game. This class uses the XNA ContentManager
    /// to manage the content.
    /// </summary>
    public class SoundResourceManagerXNA : ISoundResourceManager
    {
        /// <summary>
        /// The parent sound controller.
        /// </summary>
        /// <value>Sound Controller</value>
        public SoundController SoundController { get; set; }

        /// <summary>
        /// The root directory of where the resources are being loaded from.
        /// </summary>
        /// <value>Directory name</value>
        public string RootDirectory { get { return Content.RootDirectory; } }

        /// <summary>
        /// XNA ContentManager.
        /// </summary>
        /// <value>ContentManager</value>
        ContentManager Content { get; set; }

        /// <summary>
        /// Returns the audio listener.
        /// </summary>
        public object SourceListener { get { return Listener; } }

        static AudioListener Listener = new AudioListener();

        /// <summary>
        /// Instantiates the SoundResourceManager with an XNA Content Manager.
        /// </summary>
        /// <param name="ContentManager">ContentManager</param>
        public SoundResourceManagerXNA(ContentManager ContentManager)
        {
            Content = ContentManager;
        }

        /// <summary>
        /// Returns a sound asset as a SoundEffect.
        /// </summary>
        /// <param name="AssetName">Name of the asset</param>
        /// <returns>SoundEffect (must be cast after retrieving)</returns>
        public IMaxSound GetSoundAsset(bank.SoundBank Bank, string AssetName)
        {
            return new MaxSoundXNA(Bank,Content.Load<SoundEffect>(AssetName));
        }

        /// <summary>
        /// Returns an audio emitter instance.
        /// </summary>
        /// <returns>Audio emitter</returns>
        public object GenerateEmitter()
        {
            return new AudioEmitter();
        }

        /// <summary>
        /// Updates an emitter's position.
        /// </summary>
        /// <param name="Emitter">Emitter</param>
        /// <param name="Position">Position</param>
        public void UpdateEmitterPosition(object Emitter, Vector3 Position)
        {
            if(!(Emitter is AudioEmitter))
            {
                throw new System.ArgumentException("Emitter must be of type " + typeof(AudioEmitter).FullName);
            }

            AudioEmitter emitter = (AudioEmitter)Emitter;
            emitter.Position = Position;
        }

        /// <summary>
        /// Updates the position of the sound listener.
        /// </summary>
        public void Update()
        {
            if (SoundController.MovedThisFrame)
            {
                Listener.Position = new Vector3(SoundController.Position.X, SoundController.Position.Y, 0);
            }
        }
    }
}
