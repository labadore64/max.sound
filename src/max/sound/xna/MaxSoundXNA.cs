﻿using max.sound.bank;
using max.sound.effect;
using max.sound.resource;
using Microsoft.Xna.Framework.Audio;

namespace max.sound.xna
{
    /// <summary>
    /// A wrapper class for XNA's SoundEffect class so XNA
    /// audio can be used with the SoundController.
    /// </summary>
    public class MaxSoundXNA : IMaxSound
    {
        #region Properties

        /// <summary>
        /// The source object.
        /// </summary>
        /// <value>XNA SoundEffect</value>
        public object Source { get { return Sound;  } }

        /// <summary>
        /// The name of the sound effect. Usually its filename.
        /// </summary>
        /// <value>Name</value>
        public string Name 
        { 
            get 
            { 
                if (Sound != null) 
                { 
                    return Sound.Name; 
                } 
                return ""; 
            } 
        }

        public bank.SoundBank SoundBank { get; protected set; }

        public ISoundResourceManager Parent { get { return SoundBank.SoundController.ResourceManager; } }

        /// <summary>
        /// The sound object.
        /// </summary>
        SoundEffect Sound { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Empty constructor.
        /// </summary>
        public MaxSoundXNA()
        {

        }

        /// <summary>
        /// Create a sound with a refrence to the parent controller.
        /// </summary>
        /// <param name="Parent">Parent controller</param>
        public MaxSoundXNA(bank.SoundBank Parent)
        {
            SoundBank = Parent;
        }

        /// <summary>
        /// Create a sound from an XNA sound effect.
        /// </summary>
        /// <param name="Parent">Parent controller</param>
        /// <param name="Sound">Sound effect</param>
        public MaxSoundXNA(bank.SoundBank Parent, SoundEffect Sound)
        {
            SoundBank = Parent;
            this.Sound = Sound;
        }

        #endregion

        #region Control

        /// <summary>
        /// Create an instance of this sound effect.
        /// </summary>
        /// <returns>Sound effect instance</returns>
        public MaxSoundInstance CreateInstance()
        {
            MaxSoundInstanceXNA instance = new MaxSoundInstanceXNA(SoundBank, this);
            return instance;
        }

        /// <summary>
        /// Disposes the resources of this sound effect.
        /// Sounds must be disposed of or else they can
        /// cause memory leaks.
        /// </summary>
        public void Dispose()
        {
            if(Sound != null)
            {
                Sound.Dispose();
            }
        }

        #endregion
    }
}
