﻿using Microsoft.Xna.Framework.Audio;
using max.sound.resource;
using System;
using max.sound.effect;
using max.sound.bank;

namespace max.sound.xna
{
    /// <summary>
    /// Represents a wrapper for an XNA sound effect that can be manipulated
    /// by the sound controller.
    /// </summary>
    public class MaxSoundInstanceXNA : MaxSoundInstance
    {
        internal SoundEffect SoundEffect { get; set; }
        SoundEffectInstance Sound;

        #region Properties

        public override SoundState State
        {
            get
            {
                if(Sound.State == Microsoft.Xna.Framework.Audio.SoundState.Playing)
                {
                    return SoundState.PLAYING;
                }
                else if (Sound.State == Microsoft.Xna.Framework.Audio.SoundState.Paused)
                {
                    return SoundState.PLAYING;
                }

                return SoundState.STOPPED;
            }
        }

        /// <summary>
        /// Pitch of the sound effect.
        /// </summary>
        /// <value>Pitch</value>
        public override float Pitch { get { return Sound.Pitch; } set { Sound.Pitch = value; } }
        /// <summary>
        /// Pan of the sound effect.
        /// </summary>
        /// <value>Pan</value>
        public override float Pan { get { return Sound.Pan; } set { Sound.Pan = value; } }
        /// <summary>
        /// Volume of the sound effect.
        /// </summary>
        /// <value>Volume</value>
        public override float Volume { get { return Sound.Volume; } set { Sound.Volume = value; } }
        /// <summary>
        /// Whether or not the sound is looped.
        /// </summary>
        /// <value>True/False</value>
        public override bool IsLooped { get { return Sound.IsLooped; } set { Sound.IsLooped = value; } }

        /// <summary>
        /// The name of the sound effect. Usually its filename.
        /// </summary>
        /// <value>Name</value>
        public override string Name { get { return SoundEffect.Name; } }

        #endregion

        #region Constructors

        /// <summary>
        /// Empty constructor.
        /// </summary>
        public MaxSoundInstanceXNA()
        {

        }

        /// <summary>
        /// Initialize sound with a specific sound effect
        /// </summary>
        /// <param name="Sound"></param>
        public MaxSoundInstanceXNA(bank.SoundBank Parent, IMaxSound Sound)
        {
            SoundResource = Sound;
            SoundEffect = (SoundEffect)Sound.Source;
            Initialize(Parent, SoundEffect);
        }

        #endregion

        #region Controller

        /// <summary>
        /// Initializes the sound effect.
        /// </summary>
        /// <param name="SoundData">The object used to initialize the sound</param>
        private void Initialize(bank.SoundBank Parent, object SoundData)
        {
            this.Parent = Parent;
            if(SoundData.GetType() != typeof(SoundEffect))
            {
                throw new ArgumentException("Argument must be of type Microsoft.Xna.Framework.Audio.SoundEffect");
            }
            SoundEffect Effect = (SoundEffect)SoundData;
            Sound = Effect.CreateInstance();
        }

        public override void Update()
        {
            if (DisposeWhenStopped)
            {
                if(State == SoundState.STOPPED)
                {
                    Dispose();
                }
            }
        }

        /// <summary>
        /// Disposes the sound resources for this sound effect. Use this before releasing resources to avoid memory leaks.
        /// </summary>
        public override void Dispose()
        {
            Sound.Stop();
            Sound.Dispose();
            Disposed = true;
        }

        /// <summary>
        /// Apply 3D sound to the sound effect using an XNA AudioListener and XNA AudioEmitter. 
        /// </summary>
        /// <param name="args">[AudioListener, AudioEmitter]</param>
        public override void Apply3D(params object[] args)//AudioListener Listener,AudioEmitter Emitter)
        {
            string errors = "";

            if (args.Length != 2)
            {
                errors += "Must have 2 arguments - Microsoft.Xna.Framework.Audio.AudioListener, Microsoft.Xna.Framework.Audio.AudioEmitter. ";
            }
            if (args[0].GetType() != typeof(AudioListener))
            {
                errors += "1st argument must be of type Microsoft.Xna.Framework.Audio.AudioListener. ";
            }
            if (args[1].GetType() != typeof(AudioEmitter))
            {
                errors += "2nd argument must be of type Microsoft.Xna.Framework.Audio.AudioEmitter. ";
            }

            if (errors.Length > 0)
            {
                throw new ArgumentException(errors);
            }
            AudioListener Listener = (AudioListener)args[0];
            AudioEmitter Emitter = (AudioEmitter)args[1];
            Sound.Apply3D(Listener, Emitter);
        }

        #endregion

        #region Sound

        /// <summary>
        /// Pauses this sound effect.
        /// </summary>
        public override void Pause()
        {
            if (State != SoundState.PAUSED)
            {
                Sound.Pause();
            }
        }

        /// <summary>
        /// Play this sound effect.
        /// </summary>
        public override void Play()
        {
            if (State != SoundState.PLAYING)
            {
                Sound.Play();
            }
        }

        /// <summary>
        /// Stop this sound effect.
        /// </summary>
        public override void Stop()
        {
            if (State != SoundState.STOPPED)
            {
                Sound.Stop();
            }
        }

        #endregion

        #region Other
        public override MaxSoundInstance Copy()
        {
            return new MaxSoundInstanceXNA(Parent,SoundResource);
        }
        #endregion
    }
}
