# Using max.sound

``max.sound`` is a MaxLib Game Library that manages sound effects, 3D sound and music.

* [Installation](Installation.md)

## Max Dependencies:

* [max.geometry](https://labadore64.gitlab.io/max.geometry/)
* [max.serialize](https://labadore64.gitlab.io/max.serialize/)