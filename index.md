# max.sound

``max.sound`` is a MaxLib Game Library that manages sound effects, 3D sound and music.

* [Documentation](https://labadore64.gitlab.io/max.sound/)
* [Source](https://gitlab.com/labadore64/max.sound/)